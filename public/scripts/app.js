"use strict";

// JSX

var badge = React.createElement(
  "div",
  null,
  React.createElement("img", { src: "/images/arion_badge.png", width: "25px", height: "auto" })
);
var templateOne = React.createElement(
  "div",
  null,
  React.createElement(
    "nav",
    { className: "navbar navbar-expand-lg navbar-dark bg-primary" },
    React.createElement(
      "a",
      { className: "navbar-brand", href: "#" },
      badge
    ),
    React.createElement(
      "button",
      { className: "navbar-toggler", type: "button", "data-toggle": "collapse", "data-target": "#navbarColor01", "aria-controls": "navbarColor01", "aria-expanded": "false", "aria-label": "Toggle navigation" },
      React.createElement("span", { className: "navbar-toggler-icon" })
    ),
    React.createElement(
      "div",
      { className: "collapse navbar-collapse", id: "navbarColor01" },
      React.createElement(
        "ul",
        { className: "navbar-nav mr-auto" },
        React.createElement(
          "li",
          { className: "nav-item active" },
          React.createElement(
            "a",
            { className: "nav-link btn btn-outline-secondary", href: "#" },
            "Home ",
            React.createElement(
              "span",
              { className: "sr-only" },
              "(current)"
            )
          )
        ),
        React.createElement(
          "li",
          { className: "nav-item" },
          React.createElement(
            "a",
            { className: "nav-link btn btn-outline-secondary disabled", href: "#" },
            "Features"
          )
        ),
        React.createElement(
          "li",
          { className: "nav-item" },
          React.createElement(
            "a",
            { className: "nav-link btn btn-outline-secondary disabled", href: "#" },
            "Pricing"
          )
        ),
        React.createElement(
          "li",
          { className: "nav-item" },
          React.createElement(
            "a",
            { className: "nav-link btn btn-outline-secondary disabled", href: "#" },
            "About"
          )
        )
      )
    )
  )
);

var construction = "Page Under Construction";
var templateTwo = React.createElement(
  "div",
  null,
  React.createElement("img", { src: "/images/arion_logo_edited.png" }),
  React.createElement(
    "h1",
    { className: "text-warning text-center" },
    construction
  ),
  React.createElement(
    "blockquote",
    { className: "blockquote text-center" },
    React.createElement(
      "p",
      { "class": "mb-0" },
      "Global Network Consulting and Integration"
    ),
    React.createElement(
      "footer",
      { className: "blockquote-footer" },
      "find us on Wechat: \xA0",
      React.createElement(
        "cite",
        { title: "Source Title" },
        "arion.com"
      )
    )
  )
);

var appHeader = document.getElementById('header');
var appRoot = document.getElementById('app');

ReactDOM.render(templateOne, appHeader);
ReactDOM.render(templateTwo, appRoot);
