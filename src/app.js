// JSX

const badge = (
  <div>
    <img src="/images/arion_badge.png" width="25px" height="auto" />
  </div>
);
const templateOne = (
  <div>
        <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
      <a className="navbar-brand" href="#">{badge}</a>
      <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
        <span className="navbar-toggler-icon"></span>
      </button>

      <div className="collapse navbar-collapse" id="navbarColor01">
        <ul className="navbar-nav mr-auto">
          <li className="nav-item active">
            <a className="nav-link btn btn-outline-secondary" href="#">Home <span className="sr-only">(current)</span></a>
          </li>
          <li className="nav-item">
            <a className="nav-link btn btn-outline-secondary disabled" href="#">Features</a>
          </li>
          <li className="nav-item">
            <a className="nav-link btn btn-outline-secondary disabled" href="#">Pricing</a>
          </li>
          <li className="nav-item">
            <a className="nav-link btn btn-outline-secondary disabled" href="#">About</a>
          </li>
        </ul>
{/*        <form className="form-inline my-2 my-lg-0">
          <input className="form-control mr-sm-2" type="text" placeholder="Search" />
          <button className="btn btn-secondary my-2 my-sm-0" type="submit">Search</button>
        </form>*/}
      </div>
    </nav>
  </div>
);

const construction = "Page Under Construction";
const templateTwo = (
  <div>
    <img src="/images/arion_logo_edited.png" />
    <h1 className="text-warning text-center">{construction}</h1>
    <blockquote className="blockquote text-center">
      <p class="mb-0">Global Network Consulting and Integration</p>
      <footer className="blockquote-footer">
        find us on Wechat: &nbsp; 
        <cite title="Source Title">arion.com</cite></footer>
    </blockquote>
  </div>
);

const appHeader = document.getElementById('header');
const appRoot = document.getElementById('app');

ReactDOM.render(templateOne, appHeader);
ReactDOM.render(templateTwo, appRoot);
